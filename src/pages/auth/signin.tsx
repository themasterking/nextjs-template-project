import React, { ReactElement } from 'react';
import { Button } from 'components';

export default function Signin(): ReactElement {
    return (
        <div>
            <p>Signin</p>
            <Button color="primary" title="Click me" />
        </div>
    );
}
