import React from 'react';
import { render } from '@testing-library/react';
import { screen } from '@testing-library/dom';
import { Button } from './Button';

test('scenario Button', () => {
    render(<Button color="primary" title="Click me" />);
    const title = screen.getByText('Click me primary');
    expect(title).toBeInTheDocument();
});
