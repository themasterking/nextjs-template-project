import React, { FC } from 'react';
import { ButtonStyle } from './Button.style';

interface Props {
    color: string;
    title: string;
}

export const Button: FC<Props> = ({ color, title }) => {
    return (
        <ButtonStyle>
            {title} {color}
        </ButtonStyle>
    );
};
