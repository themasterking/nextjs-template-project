import { ComponentStory, ComponentMeta } from '@storybook/react';
import { TextField } from './TextField';

export default {
    title: 'UI/Form/TextField',
    component: TextField,
} as ComponentMeta<typeof TextField>;

const Template: ComponentStory<typeof TextField> = (args) => {
    return <TextField {...args} />;
};

export const Default = Template.bind({});

export const Email = Template.bind({
    type: 'email',
});
